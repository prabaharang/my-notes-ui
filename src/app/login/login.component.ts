import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { RouterService } from '../services/router.service';
import { AuthService } from '../services/authentication.service';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatInput } from '@angular/material/input';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements AfterViewInit {
  username = new FormControl();
  password = new FormControl();
  submitMessage: string;

  @ViewChild('usernameInput') usernameInput: MatInput;

  constructor(
    private routerService: RouterService,
    private authService: AuthService,
    public snackbar: MatSnackBar
  ) {

  }

  ngAfterViewInit() {
    setTimeout(() => this.usernameInput.focus());
  }

  loginSubmit() {
    if (this.username.value && this.password.value) {
      this.authService.authenticateUser({
        username: this.username.value,
        password: this.password.value
      }).subscribe(
        (data: any) => {
          this.authService.setBearerToken(data.token);
          this.snackbar.open(`Login successful. Welcome ${data.user.username}!`, null, { duration: 2000 });
          this.routerService.routeToNoteView();
        },
        (err: HttpErrorResponse) => {
          if (err.status === 404) {
            this.submitMessage = 'Http failure response for http://localhost:3000/auth/v1: 404 Not Found';
          }
          if (err.status === 403) {
            this.submitMessage = err.error.message;
            this.snackbar.open(err.error.message, null, { duration: 2000 });
          }
        }
      );
    }
  }

  register() {
    this.routerService.routeToRegister();
  }
}
