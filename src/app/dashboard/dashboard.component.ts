import { Component, OnInit } from '@angular/core';
import { NotesService } from '../services/notes.service';
import { ReminderService } from '../services/reminder.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ReminderComponent } from '../reminder/reminder.component';
import { AuthService } from '../services/authentication.service';
import { ShareService } from '../services/share.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  errMessage: string;

  constructor(
    private notesService: NotesService,
    private shareService: ShareService,
    private reminderService: ReminderService,
    private authService: AuthService,
    public snackbar: MatSnackBar
  ) {

  }

  ngOnInit() {
    this.getNotes();
    this.initializeShare();
  }

  getNotes() {
    this.notesService.fetchNotesFromServer().subscribe();
    this.loadReminders();
  }

  initializeShare() {
    this.authService.getUser().subscribe(user => {
      if (user) {
        this.shareService.connect(user).subscribe(data => {
          this.notesService.fetchNotesFromServer().subscribe();
          this.snackbar.open(
            `${data.username} shared ${data.notes.length} note(s).`,
            null, { duration: 2000 }
          );
        });
      }
    });
  }

  private loadReminders() {
    this.notesService.getNotes().subscribe(notes => {
      if (notes.length) {
        notes.filter(note => note.reminder).map(note => {
          this.reminderService.setReminder(note, 15).subscribe(this.showReminder.bind(this));
          this.reminderService.setReminder(note).subscribe(this.showReminder.bind(this));
        });
      }
    });
  }

  private showReminder(note) {
    this.snackbar.openFromComponent(ReminderComponent, { data: note });
  }
}
