import { Component, Input, OnInit } from '@angular/core';
import { Note } from '../note';
import { RouterService } from '../services/router.service';
import { AuthService } from '../services/authentication.service';
import { NotesService } from '../services/notes.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.css']
})
export class NoteComponent implements OnInit {
  @Input() note: Note;
  user: any;

  constructor(
    private notesService: NotesService,
    private routerService: RouterService,
    private authService: AuthService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.authService.getUser().subscribe(user => {
      if (user) {
        this.user = user;
      }
    });
  }

  editNote() {
    this.routerService.routeToEditNoteView(this.note.id);
  }

  removeGroup(note: Note){
    note.group ='';
    this.notesService.editNote(note).subscribe(
      data =>{},
      err => {}
    );
    
  }
  get showCheckbox() {
    return this.router.url.includes('view/noteview') && this.note.userId === this.user.userId;
  }
  get isShared(){
    return this.note.userId !== this.user.userId;
  }
  get editable() {
    let editable: boolean;
    if (this.user && this.note) {
      editable = this.user.userId === this.note.userId
        || !!this.note.sharedWith.find((shared: any) => shared.email === this.user.email && shared.accessType === 'write');
    }
    return editable;
  }
}
