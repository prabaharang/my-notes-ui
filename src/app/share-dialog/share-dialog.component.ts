import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { ShareService } from '../services/share.service';
import { Note } from '../note';
import { AuthService } from '../services/authentication.service';
import { NotesService } from '../services/notes.service';

@Component({
  selector: 'app-share-dialog-view',
  templateUrl: './share-dialog.component.html',
  styleUrls: [ './share-dialog.component.css' ]
})
export class ShareDialogComponent implements OnInit {
  public form: FormGroup;

  constructor(
    private dialogRef: MatDialogRef<ShareDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { username, notes: Note[] },
    private notesService: NotesService,
    private shareService: ShareService,
    public snackbar: MatSnackBar
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl(''),
      accessType: new FormControl('read')
    });
  }

  async share() {
    try {
      const response = await this.notesService.share(
        this.form.value.email,
        this.data.notes.map(note => note.id),
        this.form.value.accessType
      );

      this.shareService.share({
        email: this.form.value.email,
        username: this.data.username,
        notes: this.data.notes.map(note => note.title)
      });
      this.snackbar.open(response.message, null, { duration: 2000 });
      this.dialogRef.close();
    } catch (err) {
    }
  }
}
