export class DateTime {
  constructor(public date?: Date, public time?: string) {
    if (!time && date && (date.getHours() || date.getMinutes())) {
      this.time = DateTime.getFormattedTime(date.getHours(), date.getMinutes());
    }
  }

  public static getOffsetTime(interval = 1) {
    const date = new Date();
    let hours: any = date.getHours();
    let minutes: any = date.getMinutes();

    minutes = -~(minutes / interval) * interval;
    // handle 55-59
    if (minutes === 60) {
      hours++;
      minutes = 0;
    }
    return DateTime.getFormattedTime(hours, minutes);
  }

  public static getFormattedTime(hours: number, minutes: number) {
    let meridiem = 'AM';
    // handle 13-23
    if (hours > 12) {
      hours -= 12;
      meridiem = 'PM';
    }

    const sHours = `0${hours}`.slice(-2);
    const sMinutes = `0${minutes}`.slice(-2);
    return `${sHours}:${sMinutes} ${meridiem}`;
  }

  toDate(): Date {
    let date: Date;
    let timeSplit, timeContent, meridiem: string;
    let hours: number, minutes: number;

    if (this.date && this.time) {
      date = new Date(this.date.getTime());
      timeSplit = this.time.split(' ');
      timeContent = timeSplit[0].split(':');
      meridiem = timeSplit[1];
      hours = +timeContent[0];
      minutes = +timeContent[1];
      if (meridiem.toUpperCase() === 'PM') {
        hours += 12;
      }
      date.setHours(hours);
      date.setMinutes(minutes);
    }

    return date;
  }

  toDateString(): string {
    let dateString: string;

    if (this.date && this.time) {
      dateString = `${this.date.getMonth() + 1}-${this.date.getDate()}-${this.date.getFullYear()} ${this.time}`;
    }
    return dateString;
  }
}
