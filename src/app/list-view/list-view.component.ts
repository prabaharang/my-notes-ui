import { Component, OnInit } from '@angular/core';
import { Note } from '../note';
import { NotesService } from '../services/notes.service';

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.css']
})
export class ListViewComponent implements OnInit {

  notStartedNotes: Array<Note> = [ ];
  startedNotes: Array<Note> = [ ];
  completedNotes: Array<Note> = [ ];

  constructor(private notesService: NotesService) {

  }

  ngOnInit() {
    this.notesService.getNotes()
      .subscribe(notes => {
        notes.forEach(note => {
          switch (note.state) {
            case 'not-started':
              this.notStartedNotes.push(note);
            break;
            case 'started':
              this.startedNotes.push(note);
            break;
            case 'completed':
            this.completedNotes.push(note);
            break;
          }
        });
      });
  }
}
