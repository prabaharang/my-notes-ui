import { DateTime } from './date-time';

export class Note {
  id?: string;
  title: string;
  text: string;
  state?: string;
  selected?: boolean;
  favourite?: boolean;
  group?: string;
  userId?: string;
  reminder?: DateTime;
  sharedWith?: [];
  snooze?: boolean;
  noteTileBgColor: string;

  constructor() {
    this.title = '';
    this.text = '';
    this.state = 'not-started';
    this.noteTileBgColor = 'red';
  }
}
