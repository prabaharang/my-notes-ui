import { Component, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA, MatSnackBarRef } from '@angular/material/snack-bar';
import { ReminderService } from '../services/reminder.service';

@Component({
  selector: 'app-reminder',
  templateUrl: './reminder.component.html',
  styleUrls: ['./reminder.component.css']
})
export class ReminderComponent {
  constructor(
    public snackbarRef: MatSnackBarRef<ReminderComponent>,
    @Inject(MAT_SNACK_BAR_DATA) public data: any,
    private reminderService: ReminderService
  ) {

  }

  snooze() {

  }

  dismiss() {
    this.snackbarRef.dismiss();
  }
}
