import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatMenuModule } from '@angular/material/menu';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatBadgeModule} from '@angular/material/badge';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { NoteViewComponent, DialogAddGroup } from './note-view/note-view.component';
import { ListViewComponent } from './list-view/list-view.component';
import { EditNoteOpenerComponent } from './edit-note-opener/edit-note-opener.component';
import { DateTimeInputComponent } from './datetime-input/datetime-input.component';
import { ReminderComponent } from './reminder/reminder.component';

import { NotesService } from './services/notes.service';
import { RouterService } from './services/router.service';
import { AuthService } from './services/authentication.service';
import { ReminderService } from './services/reminder.service';
import { CanActivateRouteGuard } from './can-activate-route.guard';
import { NoteTakerComponent } from './note-taker/note-taker.component';
import { NoteComponent } from './note/note.component';
import { EditNoteViewComponent } from './edit-note-view/edit-note-view.component';
import { MatOptionModule, MatNativeDateModule } from '../../node_modules/@angular/material/core';
import { MatSelectModule } from '../../node_modules/@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatGridListModule } from '@angular/material/grid-list';
import {MatChipsModule} from '@angular/material/chips';
import {MatTooltipModule} from '@angular/material/tooltip';

import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
//import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';

import { appRoutes } from './app.routes';
import { ShareService } from './services/share.service';
import { ShareDialogComponent } from './share-dialog/share-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    LoginComponent,
    RegistrationComponent,
    NoteViewComponent,
    DialogAddGroup,
    NoteTakerComponent,
    NoteComponent,
    ListViewComponent,
    EditNoteOpenerComponent,
    EditNoteViewComponent,
    DateTimeInputComponent,
    ReminderComponent,
    ShareDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,

    RouterModule.forRoot(appRoutes),
    OwlDateTimeModule,
    OwlNativeDateTimeModule ,
    //NgxMaterialTimepickerModule.forRoot(),

    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,

    MatToolbarModule,
    MatMenuModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatButtonModule,
    MatBadgeModule,
    MatInputModule,
    MatCardModule,
    MatOptionModule,
    MatSelectModule,
    MatDialogModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatListModule,
    MatIconModule,
    MatGridListModule,
    MatChipsModule,
    MatTooltipModule,
    FlexLayoutModule
  ],
  providers: [
    NotesService,
    RouterService,
    AuthService,
    ReminderService,
    ShareService,
    CanActivateRouteGuard
  ],
  bootstrap: [ AppComponent ],
  entryComponents: [
    EditNoteViewComponent,
    ReminderComponent,
    ShareDialogComponent,
    DialogAddGroup
  ]
})
export class AppModule { }
