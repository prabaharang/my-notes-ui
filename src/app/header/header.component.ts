import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterService } from '../services/router.service';
import { AuthService } from '../services/authentication.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NotesService } from '../services/notes.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isNoteView = true;
  username: string;
  searchText = '';

  constructor(
    private notesService: NotesService,
    private router: RouterService,
    private authService: AuthService,
    public snackbar: MatSnackBar
  ) {
  }

  ngOnInit() {
    this.authService.getUser().subscribe(user => {
      this.username = user ? user.username : '';
    });
  }

  switchToListView() {
    this.isNoteView = false;
    this.router.routeToListView();
  }

  switchToNoteView() {
    this.isNoteView = true;
    this.router.routeToNoteView();
  }

  async search() {
    if (this.searchText.trim()) {
      await this.notesService.search(this.searchText);
    }
    else{
      await this.notesService.getAllNotes();
    }
  }

  logout() {
    this.authService.logout();
    this.snackbar.open('Logout successful', null, { duration: 2000 });
    this.router.routeToLogin();
  }
}
