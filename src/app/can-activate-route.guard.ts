import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './services/authentication.service';

@Injectable()
export class CanActivateRouteGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router
  ) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> {
    return this.authService.isUserAuthenticated(this.authService.getBearerToken())
    .then(data => new Promise<boolean>((resolve, reject) => {
      if (data) {
        resolve(data);
      } else {
        this.router.navigate(['/login']);
        reject(data);
      }
    })).catch(err => new Promise<boolean>((resolve, reject) => {
      this.router.navigate(['/login']);
      reject(false);
    }));
  }
}
