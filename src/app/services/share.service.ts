import { Injectable, NgZone } from '@angular/core';
import * as io from 'socket.io-client';
import { environment } from '../../environments/environment';
import { Observable, Subscriber } from 'rxjs';

@Injectable()
export class ShareService {
  private socket: SocketIOClient.Socket;

  constructor(private ngZone: NgZone) {
  }

  connect(user): Observable<any> {
    this.ngZone.runOutsideAngular(() => {
      this.socket = io(environment.api.share);
      this.socket.emit('join', user.email);
    });
    return Observable.create((observer: Subscriber<any>) => {
      this.socket.on('share', data => {
        this.ngZone.run(() => observer.next(data));
      });
    });
  }

  share(data) {
    this.ngZone.runOutsideAngular(() => {
      this.socket.emit('share', data);
    });
  }
}
