import { Injectable } from "@angular/core";
import { Note } from "../note";
import { BehaviorSubject, Observable, Subscriber } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { AuthService } from "./authentication.service";
import { DateTime } from "../date-time";
import { environment } from "../../environments/environment";

@Injectable()
export class NotesService {
  private readonly URL = environment.api.notes;

  notes: Note[] = [];
  notesSubject: BehaviorSubject<Note[]> = new BehaviorSubject<Note[]>(
    this.notes
  );

  constructor(private http: HttpClient, private authService: AuthService) {}

  fetchNotesFromServer(): Observable<Note[]> {
    return Observable.create((observer: Subscriber<Note[]>) => {
      this.http
        .get<Note[]>(this.URL, { headers: this.authService.getAuthHeader() })
        .subscribe(notes => {
          if (notes.length) {
            notes.forEach(
              (note: any) =>
                note.reminder &&
                (note.reminder = new DateTime(new Date(note.reminder)))
            );
          }

          this.notes = notes;
          this.getNotes().next(notes);

          observer.next(notes);
          observer.complete();
        });
    });
  }

  getNotes(): BehaviorSubject<Note[]> {
    return this.notesSubject;
  }

  async addNote(note: Note) {
    const noteData = note as any;
    let savedNote: Note, _modSavedNote;
    debugger;
    if (note.reminder ) {//&& note.reminder.toDate()
      //noteData.reminder = note.reminder.toDateString();
    } else {
      delete noteData.reminder;
    }
    savedNote = await this.http
      .post<Note>(this.URL, noteData, {
        headers: this.authService.getAuthHeader()
      })
      .toPromise();

    _modSavedNote = savedNote;
    if (_modSavedNote.reminder) {
      _modSavedNote.reminder = new DateTime(new Date(_modSavedNote.reminder));
    }

    this.notes.push(_modSavedNote as Note);
    this.getNotes().next(this.notes);
  }

  editNote(note: Note): Observable<Note> {
    const index = this.notes.findIndex(oldNote => oldNote.id === note.id);
    const noteData: any = JSON.parse(JSON.stringify(note));

    this.notes[index] = note;
    this.getNotes().next(this.notes);

    if (note.reminder ) {//&& note.reminder.toDate()
      noteData.reminder = note.reminder.toDateString();
    } else {
      delete noteData.reminder;
    }
    console.log("noteData" + JSON.stringify(noteData));
    return this.http.put<Note>(`${this.URL}/${note.id}`, noteData, {
      headers: this.authService.getAuthHeader()
    });
  }

  async search(title) {
    try {
      this.notes = (await this.http
        .get(`${this.URL}/search`, {
          params: { title },
          headers: this.authService.getAuthHeader()
        })
        .toPromise()) as Note[];
      this.getNotes().next(this.notes);
    } catch (err) {
      // request failed
      console.error("Handling 404-No Results Found");
      this.getNotes().next([]);
    }
  }

  deleteNote(noteId: string): Observable<any> {
    const index = this.notes.findIndex(note => note.id === noteId);
    this.notes.splice(index, 1);
    this.getNotes().next(this.notes);

    return this.http.delete(`${this.URL}/${noteId}`, {
      headers: this.authService.getAuthHeader()
    });
  }

  deleteNotes(notesToDelete: Note[]) {
    const noteIds = notesToDelete.map(note => note.id).join(",");
    notesToDelete.forEach(noteToDelete => {
      const index = this.notes.findIndex(note => note.id === noteToDelete.id);
      this.notes.splice(index, 1);
    });
    this.getNotes().next(this.notes);

    return this.http.delete(`${this.URL}/${noteIds}`, {
      headers: this.authService.getAuthHeader()
    });
  }

  markFavourite(favouriteNotes: Note[]) {
    const noteIds = favouriteNotes.map(note => note.id).join(",");
    favouriteNotes.forEach(favouriteNote => {
      const index = this.notes.findIndex(note => note.id === favouriteNote.id);
      this.notes[index].favourite = true;
    });
    this.getNotes().next(this.notes);

    return this.http.put(`${this.URL}/favourite/${noteIds}`, null, {
      headers: this.authService.getAuthHeader()
    });
  }

  addToGroup(notesToGroup: Note[], groupName: string) {
    const noteIds = notesToGroup.map(note => note.id).join(",");
    notesToGroup.forEach(noteToGroup => {
      const index = this.notes.findIndex(note => note.id === noteToGroup.id);
      this.notes[index].group = groupName;
    });
    this.getNotes().next(this.notes);

    return this.http.put(`${this.URL}/group/${noteIds}/${groupName}`, null, {
      headers: this.authService.getAuthHeader()
    });
  }

  share(email: string, noteIds: string[], accessType: string) {
    return this.http
      .post<any>(
        `${this.URL}/share/${email}`,
        { noteIds, accessType },
        { headers: this.authService.getAuthHeader() }
      )
      .toPromise();
  }

  getNoteById(noteId): Note {
    return this.notes.find(note => note.id === noteId);
  }

  async getAllNotes() {
    console.log("getAllNotes");
    this.notes = (await this.http
      .get(`${this.URL}`, {
        headers: this.authService.getAuthHeader()
      })
      .toPromise()) as Note[];
    this.getNotes().next(this.notes);
  }
}
