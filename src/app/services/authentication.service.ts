import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable, Subscriber } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthService {
  private readonly URL = environment.api.users;
  private userSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient) {
  }

  authenticateUser(data) {
    return this.http.post(`${this.URL}/login`, data);
  }

  register(data) {
    return this.http.post(`${this.URL}/register`, data);
  }

  logout() {
    this.userSubject.next(null);
    localStorage.removeItem('bearerToken');
  }

  setBearerToken(token) {
    localStorage.setItem('bearerToken', token);
  }

  getBearerToken() {
    return localStorage.getItem('bearerToken');
  }

  isUserAuthenticated(token): Promise<boolean> {
    return this.http.post(`${this.URL}/auth`, null, {
      headers: { Authorization: `Bearer ${token}` }
    }).pipe(map((data: any) => {
      let authenticated;
      if (data && data.user) {
        authenticated = data.isAuthenticated;
        this.userSubject.next(data.user);
      }
      return authenticated;
    }))
    .toPromise();
  }

  getAuthHeader() {
    return { Authorization: `Bearer ${this.getBearerToken()}` };
  }

  getUser() {
    return this.userSubject;
  }
}
