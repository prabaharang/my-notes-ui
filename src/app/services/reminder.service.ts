import { Injectable } from '@angular/core';
import { Note } from '../note';
import { Observable, Subscriber } from 'rxjs';

@Injectable()
export class ReminderService {
  reminders: any = { };
  snoozes: any = { };

  setReminder(note: Note, offset = 0): Observable<Note> {
    return this._setReminder(note, offset, 'reminder');
  }

  setSnooze(note: Note, offset: number): Observable<Note> {
    return this._setReminder(note, offset, 'snooze');
  }

  private _setReminder(note: Note, offset = 0, type: string) {
    const now = +new Date();
    const remindTime = +note.reminder.toDate();
    const reminders = type === 'reminder' ? this.reminders : this.snoozes;
    let timerId, timeout;

    offset *= 60000;

    return Observable.create((observer: Subscriber<Note>) => {
      if ((remindTime - now) > offset) {
        clearTimeout(reminders[note.id]);
        timeout = remindTime - now - offset;
        timerId = setTimeout(() => observer.next(note), timeout);
        reminders[note.id] = timerId;
      }
    });
  }
}
