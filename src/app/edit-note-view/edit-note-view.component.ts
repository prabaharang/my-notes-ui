import { Component,  OnInit, Inject } from '@angular/core';
import { Note } from '../note';
import { NotesService } from '../services/notes.service';
import { RouterService } from '../services/router.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-edit-note-view',
  templateUrl: './edit-note-view.component.html',
  styleUrls: ['./edit-note-view.component.css']
})
export class EditNoteViewComponent implements OnInit {
  note: Note;
  states: Array<string> = ['not-started', 'started', 'completed'];
  errMessage: string;

  constructor(
    private noteService: NotesService,
    private routerService: RouterService,
    private dialogRef: MatDialogRef<EditNoteViewComponent>,
    public snackbar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) private noteId: string
  ) {

  }

  ngOnInit() {
    this.noteService.getNotes().subscribe(notes => {
      let note;
      if (notes && notes.length) {
        note = notes.find(_note => _note.id === this.noteId);
        this.note = new Note();
        Object.assign(this.note, note);
      }
    });

    this.dialogRef.afterClosed().subscribe(() => {
      this.routerService.routeBack();
    });
  }

  save() {
    this.noteService.editNote(this.note).subscribe(
      data => {
        this.dialogRef.close();
        this.snackbar.open('Note updated successfully', null, { duration: 2000 });
      },
      err => this.snackbar.open(err.message, null, { duration: 2000 })
    );
  }

  delete() {
    this.noteService.deleteNote(this.note.id).subscribe(
      res => {
        this.dialogRef.close();
        this.snackbar.open('Note deleted.', null, { duration: 2000 });
      },
      err => this.snackbar.open(err.message, null, { duration: 2000 })
    );
  }

  onDateTimeChange(dateTime) {
    setTimeout(() => this.note.reminder = dateTime);
  }
}
