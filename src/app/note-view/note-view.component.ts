import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { Note } from '../note';
import { NotesService } from '../services/notes.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../services/authentication.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ShareDialogComponent } from '../share-dialog/share-dialog.component';
import { Subscription } from 'rxjs';

export interface DialogData {
  groupName: string;
}

@Component({
  selector: 'app-note-view',
  templateUrl: './note-view.component.html',
  styleUrls: ['./note-view.component.css']
})
export class NoteViewComponent implements OnInit, OnDestroy {
  notes: Note[];
  notesSource: Note[];
  groups: string[];
  searchText = '';
  groupName = '';

  private userSub: Subscription;
  private notesSub: Subscription;

  constructor(
    private notesService: NotesService,
    private authService: AuthService,
    public snackbar: MatSnackBar,
    private dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.notesSub = this.notesService.getNotes().subscribe(
      notes => { 
        this.notes = notes;
        this.notesSource = notes;
        this.getGroups();}
    );
  }

  ngOnDestroy() {
    if (this.userSub) {
      this.userSub.unsubscribe();
    }
    if (this.notesSub) {
      this.notesSub.unsubscribe();
    }
  }

  async search() {
    if (this.searchText.trim()) {
      await this.notesService.search(this.searchText);
    }
  }

  delete() {
    const notes = this.getSelectedNotes();
    if (notes.length) {
      this.notesService.deleteNotes(notes).subscribe(
        (response: any) => {
          this.snackbar.open(response.message, null, { duration: 2000 });
        },
        (err: any) => this.snackbar.open(err.message, null, { duration: 2000 })
      );
    } else {
      this.snackbar.open('Select notes to delete.', null, { duration: 2000 });
    }
  }

  addToFavourite() {
    const notes = this.getSelectedNotes();
    if (notes.length) {
      this.notesService.markFavourite(notes).subscribe(
        (response: any) => this.snackbar.open(response.message, null, { duration: 2000 }),
        (err: any) => this.snackbar.open(err.message, null, { duration: 2000 })
      );
    } else {
      this.snackbar.open('Select notes to mark as favourite.', null, { duration: 2000 });
    }
  }

  addGroup() {
    const notes = this.getSelectedNotes();
    if (!this.groupName.trim()) {
      this.snackbar.open('Enter group name.', null, { duration: 2000 });
      return;
    }
    if (notes.length) {
      this.notesService.addToGroup(notes, this.groupName).subscribe(
        (response: any) => this.snackbar.open(response.message, null, { duration: 2000 }),
        (err: any) => this.snackbar.open(err.message, null, { duration: 2000 })
      );
    } else {
      this.snackbar.open('Select notes to add to group.', null, { duration: 2000 });
    }
  }

  addToGroup() {
    const notes = this.getSelectedNotes();
    
    if (notes.length) {
      this.openDialog(notes);
    } else {
      this.snackbar.open('Select notes to add to group.', null, { duration: 2000 });
    }
  }

  filterbyGroup(event){
    if(event){
      this.notes = this.notesSource.filter(note => note.group == event.value);
    }else{
      this.notes = this.notesSource;
    }
  }
  share() {
    let dialogRef: MatDialogRef<ShareDialogComponent>;
    const notes = this.getSelectedNotes();
    if (!notes.length) {
      this.snackbar.open('Select notes to share.', null, { duration: 2000 });
    } else {
      this.userSub = this.authService.getUser().subscribe(user => {
        if (user) {
          dialogRef = this.dialog.open(ShareDialogComponent, {
            data: { username: user.username, notes },
            width: '600px'
          });
        }
      });
    }
  }
  openDialog(notes:Note[]){
    const dialogRef = this.dialog.open(DialogAddGroup, {
      width: '350px',
      data: {groupName: this.groupName}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if(result){
      this.groupName = result;
      this.notesService.addToGroup(notes, this.groupName).subscribe(
        (response: any) => this.snackbar.open(response.message, null, { duration: 2000 }),
        (err: any) => this.snackbar.open(err.message, null, { duration: 2000 })
      );
      }
    });
  }
  private getSelectedNotes() {
    return this.notes.filter(note => note.selected);
  }

  private getGroups(){
    const grp = this.notes.map(data => data.group);
    this.groups = grp.filter((x, i, a) => x && a.indexOf(x) === i);
    
  }
}

@Component({
  selector: 'dialog-add-group',
  templateUrl: 'dialog-add-group.html',
})
export class DialogAddGroup {

  constructor(
    public dialogRef: MatDialogRef<DialogAddGroup>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}