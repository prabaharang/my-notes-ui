import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { EditNoteViewComponent } from '../edit-note-view/edit-note-view.component';

@Component({
  selector: 'app-edit-note-opener',
  templateUrl: './edit-note-opener.component.html',
  styleUrls: ['./edit-note-opener.component.css']
})
export class EditNoteOpenerComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) {

  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      setTimeout(() => {
        const dialog = this.dialog.open(EditNoteViewComponent, {
          data: params['noteId'],
          width: '800px'
        });

        // dialog.afterClosed().subscribe(result => { });
      });
    });
  }
}
