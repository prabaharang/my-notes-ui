import { Component, ViewChild, ElementRef, OnInit, AfterContentInit,
  Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { NotesService } from '../services/notes.service';
import { FormGroup, FormControl, ControlContainer } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { DateTime } from '../date-time';

@Component({
  selector: 'app-datetime-input',
  templateUrl: './datetime-input.component.html',
  styleUrls: ['./datetime-input.component.css']
})
export class DateTimeInputComponent implements OnInit, AfterContentInit, OnChanges {
  interval;

  minDate = this.getOnlyDate();
  minTime = DateTime.getOffsetTime(5);
  form: FormGroup;

  @ViewChild('dateInput') dateInput: ElementRef<HTMLInputElement>;

  @Input('input') dateTime: DateTime;
  @Output('change') change = new EventEmitter<DateTime>();

  ngOnInit() {
    this.load();
  }

  ngAfterContentInit() {
    // const timeInput = this.form.controls['time'];
    // this.interval = setInterval(() => {
    //   if (!timeInput.dirty && !this.dateTime.date) {
    //     timeInput.setValue(this.getCurrentTime());
    //   }
    // }, 30000);
    this.emitDateTime();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.dateTime = changes.dateTime.currentValue;
    this.load();
  }

  // ngOnDestroy() {
  //   clearInterval(this.interval);
  // }

  load() {
    if (!this.dateTime) {
      this.dateTime = new DateTime();
    }
    this.form = new FormGroup({
      date: new FormControl(this.dateTime.date || ''),
      time: new FormControl(this.dateTime.time || '')
    });
  }

  onDatePickerClose() {
    setTimeout(() => this.dateInput.nativeElement.blur());
  }

  onDateChange(dateInputEvent: MatDatepickerInputEvent<Date>) {
    this.emitDateTime();
  }

  onTimeChange(time) {
    this.form.controls['time'].setValue(time);
    this.emitDateTime();
  }

  private emitDateTime() {
    const { date, time } = this.form.value;
    console.log(".............................",{date, time})
    this.change.emit(new DateTime(date, time));
  }

  private getOnlyDate() {
    const date = new Date();
    date.setHours(0, 0, 0, 0);
    return date;
  }
}
