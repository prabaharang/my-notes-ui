import { Component, Input, ViewChild, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { FormControl, NgForm, Validators, FormGroup } from '@angular/forms';
import { RouterService } from '../services/router.service';
import { AuthService } from '../services/authentication.service';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatInput } from '@angular/material/input';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit, AfterViewInit {
  form: FormGroup;

  @ViewChild('username') username: MatInput;

  constructor(
    private routerService: RouterService,
    private authService: AuthService,
    public snackbar: MatSnackBar
  ) {

  }

  ngOnInit() {
    this.form = new FormGroup({
      username: new FormControl(''),
      email: new FormControl(''),
      password: new FormControl(''),
    });
  }

  ngAfterViewInit() {
    setTimeout(() => this.username.focus());
  }

  registerSubmit() {
    this.authService.register(this.form.value)
    .subscribe(
      (data: any) => {
        this.snackbar.open(`Registration successful. Welcome ${data.userInfo}!`, null, { duration: 2000 });
        this.routerService.routeToLogin();
      },
      (err: HttpErrorResponse) => {
        // if (err.status === 404) {
        //   this.submitMessage = 'Http failure response for http://localhost:3000/auth/v1: 404 Not Found';
        // }
        if (err.status === 403) {
          this.snackbar.open(err.error.message, null, { duration: 2000 });
        }
      }
    );
  }

  login() {
    this.routerService.routeToLogin();
  }
}
