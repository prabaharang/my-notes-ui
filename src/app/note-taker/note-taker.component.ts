import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Note } from '../note';
import { NotesService } from '../services/notes.service';
import { DateTime } from '../date-time';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-note-taker',
  templateUrl: './note-taker.component.html',
  styleUrls: ['./note-taker.component.css']
})
export class NoteTakerComponent implements OnInit {
  errMessage: string;
  form: FormGroup;
  reminder: DateTime;

  constructor(
    private notesService: NotesService,
    public snackbar: MatSnackBar
  ) {

  }

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl(''),
      text: new FormControl(''),
      group: new FormControl(''),
      reminder: new FormControl('')
    });
  }

  async addNote(formDirective) {
    const note: Note = this.form.value;
    //const dt:Date = new Date(note.reminder.toDateString());
    //note.reminder = new DateTime(dt, DateTime.getFormattedTime(dt.getHours(), dt.getMinutes()));//this.reminder;
    await this.notesService.addNote(note);

    this.snackbar.open('Note created successfully', null, { duration: 2000 });
    this.form.reset();
    formDirective.resetForm();
    this.reminder = null;
  }

  onDateTimeChange(dateTime) {
    setTimeout(() => this.reminder = dateTime);
  }
}
