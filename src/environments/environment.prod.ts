export const environment = {
  production: true,
  api: {
    users: 'http://localhost:3000/api/v1/users',
    notes: 'http://localhost:3001/api/v1/notes',
    share: 'http://localhost:3002'
  }
};
