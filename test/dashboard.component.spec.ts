import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { of } from 'rxjs';

import { MatSnackBarModule } from '@angular/material/snack-bar';

import { DashboardComponent } from '../src/app/dashboard/dashboard.component';
import { NotesService } from '../src/app/services/notes.service';
import { AuthService } from '../src/app/services/authentication.service';
import { ReminderService } from '../src/app/services/reminder.service';
import { ShareService } from '../src/app/services/share.service';

const testConfig = {
  notes: [{
      id: 1,
      title: 'Read Angular 5 blog',
      text: 'Shall do at 6 pm',
      state: 'not-started'
    },
    {
      id: 2,
      title: 'Call Ravi',
      text: 'Track the new submissions',
      state: 'not-started'
    }]
};

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let notesService: any;
  let spyFetchNotesFromServer: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardComponent ],
      imports: [
        HttpClientModule,
        MatSnackBarModule
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
      providers: [
        NotesService,
        ShareService,
        ReminderService,
        AuthService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    notesService = TestBed.get(NotesService);
    spyFetchNotesFromServer = spyOn(notesService, 'fetchNotesFromServer').and.returnValue(of(testConfig.notes));
  });

  it('should create', () => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    expect(component).toBeTruthy();
  });

  it('fetchNotesFromServer should be called whenever DashboardComponent is rendered', fakeAsync(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    tick();
    expect(notesService.fetchNotesFromServer).toHaveBeenCalled();
  }));
});
