import { RegistrationPage } from './page-objects/register.po';
import { browser } from 'protractor';

describe('registration page', () => {
  let page: RegistrationPage;

  beforeEach(() => {
    page = new RegistrationPage();
  });

  it('should get username input box', () => {
    page.navigateToRegister();
    expect(page.isUserNameInputBoxPresent())
    .toBeTruthy(`<input class="username" matInput [formControl]='username'> should exist in login.component.html`);
  });

  it('should get email input box', () => {
    page.navigateToRegister();
    expect(page.isEmailInputBoxPresent())
    .toBeTruthy(`<input class="email" matInput [formControl]='email'> should exist in login.component.html`);
  });

  it('should get passsword input box', () => {
    page.navigateToRegister();
    expect(page.isPasswordInputBoxPresent())
    .toBeTruthy(`<input class="password" matInput type = 'password' [formControl]='password'>
      should exist in login.component.html`);
  });

  it('should get submit button', () => {
    page.navigateToRegister();
    expect(page.isSubmitButtonPresent()).toBeTruthy('submit button should exist in login.component.html');
  });

  it('default values of username and password should be empty', () => {
    const emptyRegisterValues = ['', '', ''];
    page.navigateToRegister();
    expect(page.getRegisterInputBoxesDefaultValues()).toEqual(
      emptyRegisterValues,
      'Default values for username and password should be empty'
    );
  });

  it('should register into the system', async () => {
    let newNoteValues;
    await page.navigateToRegister();
    newNoteValues = page.addRegisterValues();
    expect(page.getRegisterInputBoxesDefaultValues()).toEqual(newNoteValues, 'Should be able to set values for username and password');
    await page.clickSubmitButton();
    expect(await page.getCurrentURL()).toContain('login', 'Should navigate to login page');
  });
});
