import { browser, by, element, ElementFinder, promise } from 'protractor';

export class RegistrationPage {
  // navigate to Register page
  navigateToRegister() {
    return browser.get('/register');
  }
  // get current URL
  getCurrentURL() {
    return browser.getCurrentUrl();
  }
  // get Register component
  getRegisterComponent(): ElementFinder {
    return element(by.tagName('app-registration'));
  }
  // get username input box
  getUserNameInputBox(): ElementFinder {
    return element(by.className('username'));
  }
  // check username input box is exist or not
  isUserNameInputBoxPresent(): promise.Promise<boolean> {
    return this.getUserNameInputBox().isPresent();
  }
  // get password input box
  getEmailInputBox(): ElementFinder {
    return element(by.className('email'));
  }
  // check Email input box is exist or not
  isEmailInputBoxPresent(): promise.Promise<boolean> {
    return this.getEmailInputBox().isPresent();
  }
  // get password input box
  getPasswordInputBox(): ElementFinder {
    return element(by.className('password'));
  }
  // check password input box is exist or not
  isPasswordInputBoxPresent(): promise.Promise<boolean> {
    return this.getPasswordInputBox().isPresent();
  }
  // get submit button
  getSubmitButton(): ElementFinder {
    return this.getRegisterComponent().element(by.buttonText('Register'));
  }
  // check submit button is present or not
  isSubmitButtonPresent(): promise.Promise<boolean> {
    return this.getSubmitButton().isPresent();
  }
  // click submit button
  clickSubmitButton(): promise.Promise<void> {
    return this.getSubmitButton().click();
  }
  // default values of input boxes
  getRegisterInputBoxesDefaultValues(): any {
    let inputUsername, inputEmail, inputPassword;
    inputUsername = this.getUserNameInputBox().getAttribute('value');
    inputEmail = this.getEmailInputBox().getAttribute('value');
    inputPassword = this.getPasswordInputBox().getAttribute('value');
    return Promise.all([inputUsername, inputEmail, inputPassword]).then(values => values);
  }
  // get username and password details
  getMockRegisterDetail(): any {
    const registerDetail: any = {
      username: 'user1',
      email: 'user1@gmail.com',
      password : 'pass1'
    };
    return registerDetail;
  }
  // set username and password input box values
  addRegisterValues(): any {
    const registerDetail: any = this.getMockRegisterDetail();
    this.getUserNameInputBox().sendKeys(registerDetail.username);
    this.getEmailInputBox().sendKeys(registerDetail.email);
    this.getPasswordInputBox().sendKeys(registerDetail.password);
    return Object.keys(registerDetail).map(key => registerDetail[key]);
  }

}
